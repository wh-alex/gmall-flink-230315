package com.atguigu.func;

import org.apache.flink.table.annotation.DataTypeHint;
import org.apache.flink.table.annotation.FunctionHint;
import org.apache.flink.table.functions.TableFunction;
import org.apache.flink.types.Row;

/**
 * hello_atguigu:hello_flink
 * ====>
 * a        b
 * hello    atguigu
 * hello    flink
 */
@FunctionHint(output = @DataTypeHint("ROW<a STRING, b STRING>"))
public class TestTableFunction extends TableFunction<Row> {

    //hello_atguigu:hello_flink
    public void eval(String keyword) {

        String[] split = keyword.split(":");
        for (String word : split) {
            String[] split1 = word.split("_");
            collect(Row.of(split1[0], split1[1]));
        }
    }
}
