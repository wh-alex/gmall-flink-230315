package com.atguigu.app;

import com.atguigu.bean.WaterSensor1;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.SlidingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class FlinkSlidingWindowTest {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        env.socketTextStream("hadoop102", 9999)
                .map(new MapFunction<String, WaterSensor1>() {
                    @Override
                    public WaterSensor1 map(String value) throws Exception {
                        String[] split = value.split(",");
                        return new WaterSensor1(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2]));
                    }
                })
                .assignTimestampsAndWatermarks(WatermarkStrategy.<WaterSensor1>forMonotonousTimestamps().withTimestampAssigner(new SerializableTimestampAssigner<WaterSensor1>() {
                    @Override
                    public long extractTimestamp(WaterSensor1 element, long recordTimestamp) {
                        return element.getTs();
                    }
                }))
                .keyBy(WaterSensor1::getId)
                .window(SlidingEventTimeWindows.of(Time.seconds(6), Time.seconds(2)))
                .reduce(new ReduceFunction<WaterSensor1>() {
                    @Override
                    public WaterSensor1 reduce(WaterSensor1 value1, WaterSensor1 value2) throws Exception {
                        //value1.setVc(value1.getVc() + value2.getVc());
                        return new WaterSensor1(value1.getId(),
                                value1.getTs(),
                                value1.getVc() + value2.getVc());
                    }
                })
                .print();

        env.execute();

    }

}
