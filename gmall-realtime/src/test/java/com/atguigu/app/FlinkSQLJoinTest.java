package com.atguigu.app;

import com.atguigu.bean.WaterSensor1;
import com.atguigu.bean.WaterSensor2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

import java.time.Duration;

public class FlinkSQLJoinTest {

    public static void main(String[] args) {

        //获取执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        System.out.println(tableEnv.getConfig().getIdleStateRetention());
        //tableEnv.getConfig().set("table.exec.state.ttl", "5s");
        tableEnv.getConfig().setIdleStateRetention(Duration.ofSeconds(10));

        //从端口读取数据
        SingleOutputStreamOperator<WaterSensor1> ds1 = env.socketTextStream("hadoop102", 8888)
                .map(line -> {
                    String[] split = line.split(",");
                    return new WaterSensor1(split[0], Long.parseLong(split[1]), Double.parseDouble(split[2]));
                });
        SingleOutputStreamOperator<WaterSensor2> ds2 = env.socketTextStream("hadoop102", 9999)
                .map(line -> {
                    String[] split = line.split(",");
                    return new WaterSensor2(split[0], Long.parseLong(split[1]), split[2]);
                });

        //将流转换为动态表
        tableEnv.createTemporaryView("t1", ds1);
        tableEnv.createTemporaryView("t2", ds2);

        //内连接        左表:onCreateAndWrite   右表:onCreateAndWrite
//        tableEnv.sqlQuery("select t1.id,t1.vc,t2.name from t1 join t2 on t1.id=t2.id")
//                .execute()
//                .print();
        //左外连接      左表:onReadAndWrite      右表:onCreateAndWrite
        tableEnv.sqlQuery("select t1.id,t1.vc,t2.id,t2.name from t1 left join t2 on t1.id=t2.id")
                .execute()
                .print();
        //右外连接      左表:onCreateAndWrite    右表:onReadAndWrite
        tableEnv.sqlQuery("select t1.id,t1.vc,t2.id,t2.name from t1 right join t2 on t1.id=t2.id")
                .execute()
                .print();
        //全外连接      左表:onReadAndWrite      右表:onReadAndWrite
        tableEnv.sqlQuery("select t1.id,t1.vc,t2.id,t2.name from t1 full join t2 on t1.id=t2.id")
                .execute()
                .print();
    }

}
