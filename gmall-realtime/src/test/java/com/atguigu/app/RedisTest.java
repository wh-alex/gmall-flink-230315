package com.atguigu.app;

import com.atguigu.utils.JedisUtil;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulRedisConnection;
import lombok.SneakyThrows;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class RedisTest {

    public static void main(String[] args) throws InterruptedException {

        StatefulRedisConnection<String, String> asyncRedisConnection = JedisUtil.getAsyncRedisConnection();

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                4,
                20,
                1,
                TimeUnit.MINUTES,
                new LinkedBlockingQueue<>());

        for (int i = 0; i < 4; i++) {
            Thread.sleep(100);
            threadPoolExecutor.execute(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    RedisFuture<String> future = asyncRedisConnection.async().get("DIM:dim_spu_info:2");
                    Thread.sleep((long) (Math.random() * 10000L));
                    System.out.println(Thread.currentThread().getName() + future.get());
                }
            });
        }
    }
}
