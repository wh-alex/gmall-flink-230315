package com.atguigu.app.func;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.bean.TableProcess;
import com.atguigu.common.Constant;
import com.atguigu.utils.JdbcUtil;
import org.apache.flink.api.common.state.BroadcastState;
import org.apache.flink.api.common.state.MapStateDescriptor;
import org.apache.flink.api.common.state.ReadOnlyBroadcastState;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.co.BroadcastProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;

public class DimDwdTableProcessFunction extends BroadcastProcessFunction<JSONObject, TableProcess, JSONObject> {

    private MapStateDescriptor<String, TableProcess> mapStateDescriptor;
    private OutputTag<JSONObject> kafkaTag;
    private HashMap<String, TableProcess> tableProcessHashMap;

    public DimDwdTableProcessFunction(MapStateDescriptor<String, TableProcess> mapStateDescriptor) {
        this.mapStateDescriptor = mapStateDescriptor;
    }

    public DimDwdTableProcessFunction(MapStateDescriptor<String, TableProcess> mapStateDescriptor, OutputTag<JSONObject> kafkaTag) {
        this.mapStateDescriptor = mapStateDescriptor;
        this.kafkaTag = kafkaTag;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化HashMap
        tableProcessHashMap = new HashMap<>();

        Connection connection = DriverManager.getConnection(Constant.MYSQL_URL, "root", "000000");
        List<TableProcess> tableProcessList = JdbcUtil.queryList(connection,
                "select * from table_process",
                TableProcess.class,
                true);

        //遍历集合,将数据放入HashMap
        for (TableProcess tableProcess : tableProcessList) {
            String sinkType = tableProcess.getSinkType();
            if ("dim".equals(sinkType)) {
                tableProcessHashMap.put(tableProcess.getSourceTable(), tableProcess);
            } else {
                tableProcessHashMap.put(tableProcess.getSourceTable() + "-" + tableProcess.getSourceType(), tableProcess);
            }
        }

        connection.close();
    }

    //value:{"database":"gmall-220623-flink","table":"comment_info","type":"insert","ts":1669162958,"xid":1111,"xoffset":13941,"data":{"id":1595211185799847960,"user_id":119,"nick_name":null,"head_img":null,"sku_id":31,"spu_id":10,"order_id":987,"appraise":"1204","comment_txt":"评论内容：48384811984748167197482849234338563286217912223261","create_time":"2022-08-02 08:22:38","operate_time":null}}
    @Override
    public void processElement(JSONObject value, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.ReadOnlyContext ctx, Collector<JSONObject> out) throws Exception {

        //获取状态数据
        ReadOnlyBroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
        String dimKey = value.getString("table");
        String type = value.getString("type");
        String dwdKey = dimKey + "-" + type;

        TableProcess dimTableProcess = broadcastState.get(dimKey);
        TableProcess mapDimTableProcess = tableProcessHashMap.get(dimKey);

        TableProcess dwdTableProcess = broadcastState.get(dwdKey);
        TableProcess mapDwdTableProcess = tableProcessHashMap.get(dwdKey);

        //维表数据
        if ((dimTableProcess != null || mapDimTableProcess != null) && (!"bootstrap-start".equals(type) && !"bootstrap-complete".equals(type))) {

            if (dimTableProcess == null) {
                dimTableProcess = mapDimTableProcess;
            }

            //列过滤
            filterColumns(value.getJSONObject("data"), dimTableProcess.getSinkColumns());

            //补充信息写出
            value.put("sinkTable", dimTableProcess.getSinkTable());
            value.put("rowKeyColumn", dimTableProcess.getSinkRowKey());
            value.put("splitKey", dimTableProcess.getSinkExtend());
            value.put("columnFamily", dimTableProcess.getSinkFamily());
            out.collect(value);
        } else {
            System.out.println("该表不是维表：" + dimKey + "或者操作类型不匹配：" + type);
        }

        //事实表数据
        if (dwdTableProcess != null || mapDwdTableProcess != null) {

            if (dwdTableProcess == null) {
                dwdTableProcess = mapDwdTableProcess;
            }

            //列过滤
            filterColumns(value.getJSONObject("data"), dwdTableProcess.getSinkColumns());
            //补充信息写出
            value.put("topic", dwdTableProcess.getSinkTable());
            //将Kafka数据写出到侧流
            ctx.output(kafkaTag, value);
        } else {
            System.out.println("该表不是事实表：" + dwdKey + "或者操作类型不匹配：" + type);
        }
    }

    /**
     * 过滤字段
     *
     * @param data        {"id":"1001","tm_name":"atguigu","logo_url":"/xx/xx"}
     * @param sinkColumns "id,tm_name"
     */
    private void filterColumns(JSONObject data, String sinkColumns) {

        //处理过滤字段
        String[] split = sinkColumns.split(",");
        List<String> columnList = Arrays.asList(split);

        Set<Map.Entry<String, Object>> entries = data.entrySet();
        entries.removeIf(next -> !columnList.contains(next.getKey()));

//        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
//        while (iterator.hasNext()) {
//            Map.Entry<String, Object> next = iterator.next();
//            if (!sinkColumns.contains(next.getKey())) {
//                iterator.remove();
//            }
//        }
    }

    @Override
    public void processBroadcastElement(TableProcess tableProcess, BroadcastProcessFunction<JSONObject, TableProcess, JSONObject>.Context ctx, Collector<JSONObject> out) throws Exception {

        //获取状态数据
        BroadcastState<String, TableProcess> broadcastState = ctx.getBroadcastState(mapStateDescriptor);
        //dim/dwd
        String sinkType = tableProcess.getSinkType();

        if ("d".equals(tableProcess.getOp())) {
            //删除状态数据
            if ("dim".equals(sinkType)) {
                String sourceTable = tableProcess.getSourceTable();
                broadcastState.remove(sourceTable);
                tableProcessHashMap.remove(sourceTable);
            } else {
                String key = tableProcess.getSourceTable() + "-" + tableProcess.getSourceType();
                broadcastState.remove(key);
                tableProcessHashMap.remove(key);
            }
        } else {
            //添加状态数据
            if ("dim".equals(sinkType)) {
                broadcastState.put(tableProcess.getSourceTable(), tableProcess);
            } else {
                broadcastState.put(tableProcess.getSourceTable() + "-" + tableProcess.getSourceType(), tableProcess);
            }
        }
    }
}
