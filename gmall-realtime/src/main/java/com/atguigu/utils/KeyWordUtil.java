package com.atguigu.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class KeyWordUtil {

    public static List<String> splitKeyword(String keyword) throws IOException {

        //创建集合用于存放结果数据
        ArrayList<String> words = new ArrayList<>();

        //创建IK分词对象  true:ik_smart贪婪模式  false:ik_max_word
        IKSegmenter ikSegmenter = new IKSegmenter(new StringReader(keyword), false);

        //获取单词
        Lexeme lexeme = ikSegmenter.next();
        while (lexeme != null) {
            String word = lexeme.getLexemeText();
            words.add(word);
            lexeme = ikSegmenter.next();
        }

        //返回集合
        return words;
    }

    public static void main(String[] args) throws IOException {

        System.out.println(splitKeyword("我是程序员"));

    }

}
