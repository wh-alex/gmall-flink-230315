package com.atguigu.utils;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.lang.reflect.InvocationTargetException;

public class WindowUtil {

    public static <T> void setWindowTime(TimeWindow window, Iterable<T> input, Collector<T> out) throws InvocationTargetException, IllegalAccessException {
        T next = input.iterator().next();
        BeanUtils.setProperty(next, "stt", DateFormatUtil.toYmdHms(window.getStart()));
        BeanUtils.setProperty(next, "edt", DateFormatUtil.toYmdHms(window.getEnd()));
        out.collect(next);
    }

}
